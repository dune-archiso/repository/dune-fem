# [`dune-fem-repository`](https://gitlab.com/dune-archiso/repository/dune-fem) for the Arch Linux

[![pipeline status](https://gitlab.com/dune-archiso/repository/dune-fem/badges/main/pipeline.svg)](https://gitlab.com/dune-archiso/repository/dune-fem/-/commits/main)
[![coverage report](https://gitlab.com/dune-archiso/repository/dune-fem/badges/main/coverage.svg)](https://gitlab.com/dune-archiso/repository/dune-fem/-/commits/main)

This is a third-party auto-updated repository with the following [tarballs](https://gitlab.com/dune-archiso/repository/dune-fem/-/raw/main/packages.x86_64). The DUNE fem modules are here.

## Usage

### Add repository

1. Import GPG key from GPG servers.

```console
[user@hostname ~]$ sudo pacman-key --recv-keys 2403871B121BD8BB
[user@hostname ~]$ sudo pacman-key --finger 2403871B121BD8BB
[user@hostname ~]$ sudo pacman-key --lsign-key 2403871B121BD8BB
```

2. Add the following code to `/etc/pacman.conf`:

```toml
[dune-fem-repository]
SigLevel = Required DatabaseOptional
Server = https://dune-archiso.gitlab.io/repository/dune-fem/$arch
```

### List packages

To show actual packages list:

```console
[user@hostname ~]$ pacman -Sl dune-fem-repository
```


### Install packages

To install package:

```console
[user@hostname ~]$ pacman -Syy
[user@hostname ~]$ pacman -S dune-vem
```

- [dune-fem](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-fem/README.md)
- [dune-vem](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-vem/README.md)
- [dune-fem-dg](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-fem-dg/README.md)